import os

import arcade

from .definitions import ASSETS_PATH
from .mission_manager import MissionManager


class Sidebar:

    def __init__(self, manager: MissionManager, width: float, height: float, font_size: float = 14):
        self.manager = manager
        self.width = width
        self.height = height
        self.font_size = font_size
        self.smaller_font_size = font_size * 0.8
        self.texture = arcade.load_texture(os.path.join(ASSETS_PATH, "sidebar.png"))
        self.objectives = []

    def draw(self, x: float, y: float):
        text_x = x - self.width * 0.4
        arcade.draw_texture_rectangle(
            center_x=x,
            center_y=y,
            width=self.width,
            height=self.height,
            texture=self.texture,
        )
        arcade.draw_text(text="Test",
                         start_x=text_x,
                         start_y=y + self.height * 0.42,
                         color=(200, 0, 0),
                         font_size=self.font_size)

        arcade.draw_text(text="Objectives:",
                         start_x=text_x,
                         start_y=y + self.height * 0.33,
                         color=(200, 0, 0),
                         font_size=self.font_size)

        objectives = list(self.manager.current.values())[:3]
        start_height = y + self.height * 0.3

        if objectives:
            # for index, objective in enumerate(objectives):
            #    # Draw objective
            pass
        else:
            arcade.draw_text(
                text="No objectives available",
                start_x=text_x,
                start_y=start_height - self.smaller_font_size * 1.5,
                color=(200, 70, 0),
                font_size=self.smaller_font_size)
