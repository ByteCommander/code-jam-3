from .objective import Objective


class MissionManager:
    """ Class that manages objectives via categories.
        Categories can be a names such as "Selfies with friends".

        The progress is stores as an integer, that could measure how
        many selfies where taken
    """

    def __init__(self):
        self.categories = {}
        self.current = {}
        self.completed = {}

    def make_objective(self, category: str, amount: int, name: str = "") -> Objective:
        """ Creates a new Objective for the given category. """

        obj = Objective(self, name, amount, category)
        self.current[category] = obj

        return obj

    def get_progress(self, category: str) -> int:
        """ Returns the progress for the given category """
        return self.categories.get(category) or 0

    def set_progress(self, category: str, progress: int) -> bool:
        """ Sets the progress for the category and returns whether the current
            objective has been completed.

            Does **not** perform any validation on the new progress value
        """
        new_progress = self.categories[category] = progress

        if new_progress > self.current[category].amount:
            self._complete(category)
            return True

        return False

    def _complete(self, completed: str):
        """ This method should be called, when the current objective for the
            given category has been completed. Adds pops the current objective
            into a list of all the completed objectives for this category
        """
        category = self.completed.get(completed) or []
        category.append(self.current.pop(completed))

        # Add possible callbacks
