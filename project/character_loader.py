import os
import xml
from typing import Dict, List, Tuple

from defusedxml import ElementTree

from .custom_animated_sprite import CustomAnimatedSprite
from .definitions import ASSETS_PATH


class CharacterLoader:

    def __init__(self):
        self.tilesets: Dict[str, TileSet] = {}

    def open_file(self, tsx_name: str) -> "TileSet":
        """
        Load TSX tileset files created by Tiled map editor.
        :param tsx_name: basename of the tsx file to load,
        without .tsx extension and relative to `ASSETS_PATH`
        """
        tsx_path = os.path.join(ASSETS_PATH, tsx_name + ".tsx")
        tileset = TileSet(ElementTree.parse(tsx_path).getroot())
        self.tilesets[tsx_name] = tileset
        return tileset

    def load_sprite(self, tileset_name: str, scale: float = 1) -> CustomAnimatedSprite:
        """
        Load animated sprites from the tilesets, including animation and collision boundary settings.
        :param tileset_name: name of a previously opened tileset
        :param scale: scale factor for all sprite tiles, default 1
        :return: A CustomAnimatedSprite instance (subclass of arcade.Sprite).
        """
        tileset: TileSet = self.tilesets.get(tileset_name)
        sprite = CustomAnimatedSprite(file_name=tileset.image_source, width=tileset.tilewidth,
                                      height=tileset.tileheight, scale=scale, columns=tileset.columns,
                                      animations=tileset.animations, collision_points=tileset.collision_points)
        return sprite


class TileSet:
    """
    Class to represent TileSets loaded from .tsx files.
    Only partial support for the file format is implemented, as necessary for handling character tile sheets.
    This includes animations and custom collision boundaries (single rectangle only).
    """

    def __init__(self, xml_node: "xml.etree.ElementTree.Element"):
        self.name = xml_node.get("name")
        self.tilewidth = int(xml_node.get("tilewidth"))
        self.tileheight = int(xml_node.get("tileheight"))
        self.spacing = int(xml_node.get("spacing", 0))
        self.margin = int(xml_node.get("margin", 0))
        self.tilecount = int(xml_node.get("tilecount"))
        self.columns = int(xml_node.get("columns"))

        self.image_source = None
        self.image_width = None
        self.image_height = None

        self.tiles: Dict[str, Tile] = {}
        self.animations: Dict[str, Tuple[str, int]] = {}  # TODO: fill in from tiles
        self.collision_points: List[Tuple[float, float]] = []

        for child in xml_node:  # type: xml.etree.ElementTree.Element

            if child.tag == "image":
                self.image_source = child.get("source")
                self.image_width = int(child.get("width"))
                self.image_height = int(child.get("height"))
                if child.get("trans", None):
                    print(f"WARNING: {self.name} TSX: ignoring unsupported image transparent color setting")
                if child.get("format", None):
                    print(f"WARNING: {self.name} TSX: ignoring unsupported embedded image data")

            elif child.tag == "tile":
                tile_id = child.get("id")
                self.tiles[tile_id] = Tile(self.name, tile_id, child.find("animation"), child.find("objectgroup"))

            else:
                print(f"WARNING: {self.name} TSX: ignoring unsupported tag: <{child.tag}>")

        collision_tiles = [tile for tile in self.tiles.values() if tile.coll_x is not None]
        if collision_tiles:
            if len(collision_tiles) == 1:
                tile = collision_tiles[0]

                # calculate collision points of loaded rectangle relative to tile center (y inverted)
                cx, cy = self.tilewidth // 2, self.tileheight // 2
                self.collision_points = [  # [(-x, -y), (-x, +y), (+x, +y), (+x, -y)]
                    (tile.coll_x - cx, cy - tile.coll_y),
                    (tile.coll_x - cx, cy - tile.coll_y - tile.coll_height),
                    (tile.coll_x - cx + tile.coll_width, cy - tile.coll_y - tile.coll_height),
                    (tile.coll_x - cx + tile.coll_width, cy - tile.coll_y),
                ]
            else:
                print(f"WARNING: {self.name} TSX: ignoring collision box - may only specify for one tile!")


class Tile:
    def __init__(self, tileset_name: str, tile_id: str, animation_node: "xml.etree.ElementTree.Element" = None,
                 collision_group_node: "xml.etree.ElementTree.Element" = None):
        self.id = tile_id

        self.animation = None
        if animation_node is not None:
            self.animation = [(frame.get("tileid"), int(frame.get("duration"))) for frame in animation_node]

        self.coll_x = None
        self.coll_y = None
        self.coll_width = None
        self.coll_height = None
        if collision_group_node:
            objects = collision_group_node.findall("object")
            if len(objects) == 1:
                obj = objects[0]
                if len(obj) == 0:
                    self.coll_x = float(obj.get("x"))
                    self.coll_y = float(obj.get("y"))
                    self.coll_width = float(obj.get("width"))
                    self.coll_height = float(obj.get("height"))
                    if self.coll_width is None or self.coll_height is None:
                        print(f"WARNING: {tileset_name} TSX: collision object width/height properties missing")
                    if obj.get("rotation", None):
                        print(f"WARNING: {tileset_name} TSX: collision object rotation property not supported")
                else:
                    print(f"WARNING: {tileset_name} TSX: only rectangles supported as collision objects")
            else:
                print(f"WARNING: {tileset_name} TSX: only one single collision object per tile supported")
