<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.2.0" name="player" tilewidth="32" tileheight="32" tilecount="12" columns="3">
 <image source="wy_sprite_walking_by_nagyzsuzsi.png" width="96" height="128"/>
 <tile id="0">
  <animation>
   <frame tileid="0" duration="200"/>
   <frame tileid="1" duration="200"/>
   <frame tileid="2" duration="200"/>
   <frame tileid="1" duration="200"/>
  </animation>
 </tile>
 <tile id="1">
  <objectgroup draworder="index">
   <object id="1" x="5" y="12" width="22" height="20"/>
  </objectgroup>
 </tile>
 <tile id="2">
  <objectgroup draworder="index">
   <object id="2" x="11.3057" y="11.8365" width="9.2887" height="17.0912">
    <ellipse/>
   </object>
  </objectgroup>
 </tile>
</tileset>
