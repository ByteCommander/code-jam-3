<?xml version="1.0" encoding="UTF-8"?>
<tileset version="1.2" tiledversion="1.2.0" name="city" tilewidth="16" tileheight="16" spacing="1" tilecount="1036" columns="37">
 <image source="roguelikeCity_magenta.png" width="628" height="475"/>
 <terraintypes>
  <terrain name="red roof" tile="0"/>
  <terrain name="black roof" tile="8"/>
  <terrain name="grey roof" tile="16"/>
  <terrain name="yellow roof" tile="24"/>
  <terrain name="grass" tile="963"/>
  <terrain name="dirt" tile="973"/>
 </terraintypes>
 <tile id="0" terrain=",,,0"/>
 <tile id="1" terrain=",,0,"/>
 <tile id="2" terrain=",,0,0"/>
 <tile id="3" terrain=",0,,0"/>
 <tile id="4" terrain="0,0,0,"/>
 <tile id="5" terrain="0,0,,0"/>
 <tile id="6" terrain="0,0,0,0"/>
 <tile id="7" terrain="0,0,0,0" probability="0.1"/>
 <tile id="8" terrain=",,,1"/>
 <tile id="9" terrain=",,1,"/>
 <tile id="10" terrain=",,1,1"/>
 <tile id="11" terrain=",1,,1"/>
 <tile id="12" terrain="1,1,1,"/>
 <tile id="13" terrain="1,1,,1"/>
 <tile id="14" terrain="1,1,1,1"/>
 <tile id="15" terrain="1,1,1,1" probability="0.1"/>
 <tile id="16" terrain=",,,2"/>
 <tile id="17" terrain=",,2,"/>
 <tile id="18" terrain=",,2,2"/>
 <tile id="19" terrain=",2,,2"/>
 <tile id="20" terrain="2,2,2,"/>
 <tile id="21" terrain="2,2,,2"/>
 <tile id="22" terrain="2,2,2,2"/>
 <tile id="23" terrain="2,2,2,2" probability="0.1"/>
 <tile id="24" terrain=",,,3"/>
 <tile id="25" terrain=",,3,"/>
 <tile id="26" terrain=",,3,3"/>
 <tile id="27" terrain=",3,,3"/>
 <tile id="28" terrain="3,3,3,"/>
 <tile id="29" terrain="3,3,,3"/>
 <tile id="30" terrain="3,3,3,3"/>
 <tile id="31" terrain="3,3,3,3" probability="0.1"/>
 <tile id="37" terrain=",0,,"/>
 <tile id="38" terrain="0,,,"/>
 <tile id="39" terrain="0,0,,"/>
 <tile id="40" terrain="0,,0,"/>
 <tile id="41" terrain="0,,0,0"/>
 <tile id="42" terrain=",0,0,0"/>
 <tile id="43" terrain="0,0,0,0" probability="0.02"/>
 <tile id="44" terrain="0,0,0,0" probability="0.05"/>
 <tile id="45" terrain=",1,,"/>
 <tile id="46" terrain="1,,,"/>
 <tile id="47" terrain="1,1,,"/>
 <tile id="48" terrain="1,,1,"/>
 <tile id="49" terrain="1,,1,1"/>
 <tile id="50" terrain=",1,1,1"/>
 <tile id="51" terrain="1,1,1,1" probability="0.02"/>
 <tile id="52" terrain="1,1,1,1" probability="0.05"/>
 <tile id="53" terrain=",2,,"/>
 <tile id="54" terrain="2,,,"/>
 <tile id="55" terrain="2,2,,"/>
 <tile id="56" terrain="2,,2,"/>
 <tile id="57" terrain="2,,2,2"/>
 <tile id="58" terrain=",2,2,2"/>
 <tile id="59" terrain="2,2,2,2" probability="0.02"/>
 <tile id="60" terrain="2,2,2,2" probability="0.05"/>
 <tile id="61" terrain=",3,,"/>
 <tile id="62" terrain="3,,,"/>
 <tile id="63" terrain="3,3,,"/>
 <tile id="64" terrain="3,,3,"/>
 <tile id="65" terrain="3,,3,3"/>
 <tile id="66" terrain=",3,3,3"/>
 <tile id="67" terrain="3,3,3,3" probability="0.02"/>
 <tile id="68" terrain="3,3,3,3" probability="0.05"/>
 <tile id="888" terrain="4,4,4,4"/>
 <tile id="889" terrain="4,4,4,4"/>
 <tile id="892" terrain="5,5,5,5"/>
 <tile id="893" terrain="5,5,5,5"/>
 <tile id="925" terrain=",,,4"/>
 <tile id="926" terrain=",,4,4"/>
 <tile id="927" terrain=",,4,"/>
 <tile id="935" terrain=",,,5"/>
 <tile id="936" terrain=",,5,5"/>
 <tile id="937" terrain=",,5,"/>
 <tile id="962" terrain=",4,,4"/>
 <tile id="963" terrain="4,4,4,4"/>
 <tile id="964" terrain="4,,4,"/>
 <tile id="972" terrain=",5,,5"/>
 <tile id="973" terrain="5,5,5,5"/>
 <tile id="974" terrain="5,,5,"/>
 <tile id="999" terrain=",4,,"/>
 <tile id="1000" terrain="4,4,,"/>
 <tile id="1001" terrain="4,,,"/>
 <tile id="1009" terrain=",5,,"/>
 <tile id="1010" terrain="5,5,,"/>
 <tile id="1011" terrain="5,,,"/>
 <wangsets>
  <wangset name="wang-roof-red" tile="-1">
   <wangedgecolor name="" color="#ff0000" tile="-1" probability="1"/>
   <wangedgecolor name="" color="#ffff7f" tile="-1" probability="1"/>
   <wangcornercolor name="" color="#ff0000" tile="-1" probability="1"/>
   <wangcornercolor name="" color="#ffff7f" tile="-1" probability="1"/>
   <wangtile tileid="0" wangid="0x22211122"/>
   <wangtile tileid="1" wangid="0x21112222"/>
   <wangtile tileid="2" wangid="0x21111122"/>
   <wangtile tileid="3" wangid="0x22211111"/>
   <wangtile tileid="4" wangid="0x11112111"/>
   <wangtile tileid="5" wangid="0x11211111"/>
   <wangtile tileid="6" wangid="0x11111111"/>
   <wangtile tileid="7" wangid="0x11111111"/>
   <wangtile tileid="37" wangid="0x22222111"/>
   <wangtile tileid="38" wangid="0x11222221"/>
   <wangtile tileid="39" wangid="0x11222111"/>
   <wangtile tileid="40" wangid="0x11112221"/>
   <wangtile tileid="41" wangid="0x11111121"/>
   <wangtile tileid="42" wangid="0x21111111"/>
   <wangtile tileid="43" wangid="0x11111111"/>
   <wangtile tileid="44" wangid="0x11111111"/>
   <wangtile tileid="74" wangid="0x22212222"/>
   <wangtile tileid="75" wangid="0x22222122"/>
   <wangtile tileid="76" wangid="0x21222122"/>
   <wangtile tileid="77" wangid="0x21222222"/>
   <wangtile tileid="78" wangid="0x22212122"/>
   <wangtile tileid="79" wangid="0x21212222"/>
   <wangtile tileid="80" wangid="0x22212121"/>
   <wangtile tileid="81" wangid="0x21222121"/>
   <wangtile tileid="111" wangid="0x22222221"/>
   <wangtile tileid="112" wangid="0x22222222"/>
   <wangtile tileid="113" wangid="0x21212121"/>
   <wangtile tileid="114" wangid="0x22212221"/>
   <wangtile tileid="115" wangid="0x22222121"/>
   <wangtile tileid="116" wangid="0x21222221"/>
   <wangtile tileid="117" wangid="0x21212221"/>
   <wangtile tileid="118" wangid="0x21212122"/>
  </wangset>
  <wangset name="wang-wall-red" tile="-1">
   <wangedgecolor name="" color="#ff0000" tile="-1" probability="1"/>
   <wangedgecolor name="" color="#00ff00" tile="-1" probability="1"/>
   <wangedgecolor name="" color="#0000ff" tile="-1" probability="1"/>
   <wangedgecolor name="" color="#ff7700" tile="-1" probability="1"/>
   <wangedgecolor name="" color="#00e9ff" tile="-1" probability="1"/>
   <wangedgecolor name="" color="#ff00d8" tile="-1" probability="1"/>
   <wangedgecolor name="" color="#ffff00" tile="-1" probability="1"/>
   <wangtile tileid="185" wangid="0x2010201"/>
   <wangtile tileid="186" wangid="0x2010101"/>
   <wangtile tileid="187" wangid="0x1010101"/>
   <wangtile tileid="188" wangid="0x1010201"/>
   <wangtile tileid="222" wangid="0x2010201"/>
   <wangtile tileid="223" wangid="0x2010301"/>
   <wangtile tileid="224" wangid="0x3010301"/>
   <wangtile tileid="225" wangid="0x3010201"/>
   <wangtile tileid="259" wangid="0x2010201"/>
   <wangtile tileid="260" wangid="0x2010401"/>
   <wangtile tileid="261" wangid="0x4010401"/>
   <wangtile tileid="262" wangid="0x4010201"/>
   <wangtile tileid="296" wangid="0x2010201"/>
   <wangtile tileid="297" wangid="0x2010501"/>
   <wangtile tileid="298" wangid="0x5010501"/>
   <wangtile tileid="299" wangid="0x5010201"/>
   <wangtile tileid="333" wangid="0x2010201"/>
   <wangtile tileid="334" wangid="0x2010601"/>
   <wangtile tileid="335" wangid="0x6010601"/>
   <wangtile tileid="336" wangid="0x6010201"/>
   <wangtile tileid="370" wangid="0x2010201"/>
   <wangtile tileid="371" wangid="0x2010701"/>
   <wangtile tileid="372" wangid="0x7010701"/>
   <wangtile tileid="373" wangid="0x7010201"/>
  </wangset>
  <wangset name="street" tile="-1">
   <wangedgecolor name="" color="#ff0000" tile="-1" probability="1"/>
   <wangedgecolor name="" color="#00ff00" tile="-1" probability="1"/>
   <wangedgecolor name="" color="#0000ff" tile="-1" probability="1"/>
   <wangcornercolor name="" color="#ff0000" tile="-1" probability="1"/>
   <wangcornercolor name="" color="#00ff00" tile="-1" probability="1"/>
   <wangcornercolor name="" color="#0000ff" tile="-1" probability="1"/>
   <wangcornercolor name="" color="#ff7700" tile="-1" probability="1"/>
   <wangtile tileid="714" wangid="0x11111111"/>
   <wangtile tileid="792" wangid="0x411140"/>
   <wangtile tileid="793" wangid="0x41114000"/>
   <wangtile tileid="794" wangid="0x11134043"/>
   <wangtile tileid="795" wangid="0x40431113"/>
   <wangtile tileid="829" wangid="0x40004111"/>
   <wangtile tileid="830" wangid="0x11400001"/>
   <wangtile tileid="831" wangid="0x13404311"/>
   <wangtile tileid="832" wangid="0x43111340"/>
   <wangtile tileid="866" wangid="0x11100"/>
   <wangtile tileid="867" wangid="0x1110000"/>
   <wangtile tileid="903" wangid="0x111"/>
   <wangtile tileid="904" wangid="0x11000001"/>
  </wangset>
 </wangsets>
</tileset>
