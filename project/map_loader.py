import os
import random
from typing import Dict

import arcade
import pytmx

from .definitions import ASSETS_PATH


class MapLoader:

    def __init__(self):
        self.maps: Dict[str, pytmx.TiledMap] = {}

    def open_file(self, tmx_name: str) -> pytmx.TiledMap:
        """
        Load TMX map files created by Tiled map editor.
        :param tmx_name: basename of the tmx file to load,
        without .tmx extension and relative to `ASSETS_PATH`
        """
        tmx_path = os.path.join(ASSETS_PATH, tmx_name) + ".tmx"
        tiled_map = pytmx.TiledMap(tmx_path, arcade_texture_loader)
        self.maps[tmx_name] = tiled_map
        return tiled_map

    def load_spritelists(self, map_name: str, scale: float = 1) -> (arcade.SpriteList, arcade.SpriteList):
        """
        Load background tiles from the map as sprites for arcade.
        :param map_name: name of previously opened map
        :param scale: scale factor for all map tiles, default 1
        :return: A SpriteList of all elements, and an additional SpriteList
        of elements with which the player collides
        """
        tmap: pytmx.TiledMap = self.maps[map_name]
        layers = arcade.SpriteList()
        colliders = arcade.SpriteList()

        for layer_index in tmap.visible_tile_layers:
            layer: pytmx.TiledTileLayer = tmap.layers[layer_index]
            for x_tile, y_tile, texture in layer.tiles():  # type: (float, float, arcade.Texture)
                x, y = tile_to_pixel(x_tile, y_tile, tmap, scale, invert_y=True)

                sprite = arcade.Sprite(center_x=x, center_y=y, scale=scale)
                sprite.append_texture(texture)
                sprite.set_texture(0)

                layers.append(sprite)
                if layer.name == "collision_layer" or layer.properties.get("collision", "false") == "true":
                    colliders.append(sprite)

        return layers, colliders

    def find_marker(self, map_name: str, obj_name: str = None, obj_type: str = None) -> [pytmx.TiledObject]:
        tmap: pytmx.TiledMap = self.maps[map_name]
        return [obj for obj in tmap.objects if
                obj_name in (obj.name, None) and obj_type in (obj.type, None)]

    def find_start(self, map_name: str, start_name: str = None, scale: float = 1) -> (float, float):
        tmap: pytmx.TiledMap = self.maps[map_name]
        starts = self.find_marker(map_name, obj_type="START", obj_name=start_name)
        if starts:
            start: pytmx.TiledObject = random.choice(starts)
            print(f"Selecting start point '{start.name}'")
            return start.x * scale, (tmap.height * tmap.tileheight - start.y) * scale
        else:
            print("No start point markers in the map data! Defaulting to center.")
            return tile_to_pixel(tmap.width // 2, tmap.height // 2, tmap, scale)


def arcade_texture_loader(filename: str, colorkey: any = None, **kwargs):
    if colorkey:
        print("Colorkey:", colorkey)
    if kwargs:
        print("Extra texture loader kwargs:", kwargs)

    def load_texture(rect: (float, float, float, float), flags: pytmx.TileFlags) -> arcade.Texture:
        x, y, w, h = rect
        if any(flags):
            print("Extra texture flags:", flags)

        texture = arcade.load_texture(filename, x, y, w, h)
        return texture

    return load_texture


def tile_to_pixel(tx: float, ty: float, tmap: pytmx.TiledMap, scale: float = 1,
                  invert_x: bool = False, invert_y: bool = False):
    return (scale * tmap.tilewidth * (tmap.width - tx - 0.5 if invert_x else tx + 0.5),
            scale * tmap.tileheight * (tmap.height - ty - 0.5 if invert_y else ty + 0.5))
