import arcade

try:
    from .window import GameWindow
except ModuleNotFoundError:
    print("Error: You must run this application as module, like 'python -m project'.")
    exit(1)

if __name__ == "__main__":
    game = GameWindow()
    game.setup()
    arcade.run()
