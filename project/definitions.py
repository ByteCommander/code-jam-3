import os

ROOT_PATH = os.path.dirname(os.path.abspath(__file__))
ASSETS_PATH = os.path.join(ROOT_PATH, "assets")
