import time

import arcade

from .character_loader import CharacterLoader
from .map_loader import MapLoader
from .mission_manager import MissionManager
from .sidebar import Sidebar

WINDOW_TITLE = "Young Songs"
SCREEN_WIDTH = 1200
SCREEN_HEIGHT = 800

MAP_NAME = "demo"
PLAYER_NAME = "player"

SCALE_PLAYER = 2
SCALE_MAP = 4

# Player movement speed
MOVEMENT_SPEED = 5

# How close the player has to be to the edge of the screen before the camera moves
VIEWPORT_MARGIN = 200

# Sidebar position
SIDEBAR_WIDTH = 200
SIDEBAR_HEIGHT = 400
SIDEBAR_X = SCREEN_WIDTH - SIDEBAR_WIDTH * 0.6
SIDEBAR_Y = SCREEN_HEIGHT - SIDEBAR_HEIGHT * 0.6


class GameWindow(arcade.Window):
    """ Main application class. """

    def __init__(self):
        super().__init__(SCREEN_WIDTH, SCREEN_HEIGHT, WINDOW_TITLE, resizable=False)

        # Player
        self.player_sprite: arcade.Sprite = None
        self.input_up = 0
        self.input_down = 0
        self.input_left = 0
        self.input_right = 0

        # Camera
        self.view_bottom = 0
        self.view_left = 0

        self.tile_loader = MapLoader()
        self.character_loader = CharacterLoader()

        self.player_list = arcade.SpriteList()
        self.layers = arcade.SpriteList()
        self.colliders = arcade.SpriteList()

        self.sidebar = None

        self.physics_engine = None

        arcade.set_background_color(arcade.color.AERO_BLUE)
        # vl, vr, vt, vb = self.get_viewport()
        # self.set_viewport(vl, vr, vt, vb)

        self.manager = MissionManager()
        self.perf_time = time.perf_counter()

    def setup(self):
        """ Set up the game and initialize the variables. """

        # Load sprites from file
        self.character_loader.open_file(PLAYER_NAME)
        self.player_sprite = self.character_loader.load_sprite(PLAYER_NAME, SCALE_PLAYER)

        # Load map
        self.tile_loader.open_file(MAP_NAME)
        self.layers, self.colliders = self.tile_loader.load_spritelists(MAP_NAME, SCALE_MAP)

        # Spawn the player
        self.player_sprite.set_position(*self.tile_loader.find_start(MAP_NAME, scale=SCALE_MAP))
        self.player_sprite.center_y += self.player_sprite.height // 2
        self.player_list.append(self.player_sprite)
        self.view_left = self.player_sprite.center_x - SCREEN_WIDTH // 2
        self.view_bottom = self.player_sprite.center_y - SCREEN_HEIGHT // 2

        # Sidebar
        self.sidebar = Sidebar(self.manager, SIDEBAR_WIDTH, SIDEBAR_HEIGHT)

        # Basic collision handling
        self.physics_engine = arcade.PhysicsEngineSimple(self.player_sprite, self.colliders)

        # Configure viewport
        self.update_camera(force=True)

    def on_draw(self):
        """
        Render the screen.
        """
        # This command has to happen before we start drawing
        arcade.start_render()

        self.layers.draw()
        self.player_sprite.draw()

        arcade.draw_polygon_outline(self.player_sprite.points, arcade.color.PINK)

        self.sidebar.draw(SIDEBAR_X + self.view_left, SIDEBAR_Y + self.view_bottom)

        self.update_fps()

    def update_fps(self):
        vl, vr, vb, vt = self.get_viewport()
        now = time.perf_counter()
        fps = 1 / (now - self.perf_time)
        arcade.draw_text(f"{fps:.1f}", vr - 30, vt - 20, arcade.color.BLACK, bold=True)
        self.perf_time = now

        arcade.draw_text(f"{self.player_sprite.center_x:.1f} | {self.player_sprite.center_y:.1f}",
                         vr - 100, vt - 40, arcade.color.RED, bold=True)

    def on_key_press(self, key: int, modifiers: int):
        if key in (arcade.key.W, arcade.key.UP):
            self.input_up = 1
        elif key in (arcade.key.S, arcade.key.DOWN):
            self.input_down = 1
        elif key in (arcade.key.D, arcade.key.RIGHT):
            self.input_right = 1
        elif key in (arcade.key.A, arcade.key.LEFT):
            self.input_left = 1
        elif key in (arcade.key.ESCAPE,):
            self.close()
        elif key in (arcade.key.F12,):
            self.debug()

    def on_key_release(self, key: int, modifiers: int):
        if key in (arcade.key.W, arcade.key.UP):
            self.input_up = 0
        elif key in (arcade.key.S, arcade.key.DOWN):
            self.input_down = 0
        elif key in (arcade.key.D, arcade.key.RIGHT):
            self.input_right = 0
        elif key in (arcade.key.A, arcade.key.LEFT):
            self.input_left = 0

    def update_player_position(self, delta_time=None):
        p = self.player_sprite
        p.last_center_x = p.center_x
        p.last_center_y = p.center_y

        # Calculate Changes
        p.change_x = (self.input_right - self.input_left) * MOVEMENT_SPEED
        p.change_y = (self.input_up - self.input_down) * MOVEMENT_SPEED

        # Apply changes
        self.player_sprite.update_animation()

    def update_camera(self, delta_time=None, force=False):
        # Check if we need to change the viewport
        changed = False

        # Scroll left
        left_boundary = self.view_left + VIEWPORT_MARGIN
        if self.player_sprite.left < left_boundary:
            self.view_left -= left_boundary - self.player_sprite.left
            changed = True

        # Scroll right
        right_boundary = self.view_left + SCREEN_WIDTH - VIEWPORT_MARGIN
        if self.player_sprite.right > right_boundary:
            self.view_left += self.player_sprite.right - right_boundary
            changed = True

        # Scroll up
        top_boundary = self.view_bottom + SCREEN_HEIGHT - VIEWPORT_MARGIN
        if self.player_sprite.top > top_boundary:
            self.view_bottom += self.player_sprite.top - top_boundary
            changed = True

        # Scroll down
        bottom_boundary = self.view_bottom + VIEWPORT_MARGIN
        if self.player_sprite.bottom < bottom_boundary:
            self.view_bottom -= bottom_boundary - self.player_sprite.bottom
            changed = True

        if changed or force:
            arcade.set_viewport(self.view_left, SCREEN_WIDTH + self.view_left, self.view_bottom,
                                SCREEN_HEIGHT + self.view_bottom)

    def on_update(self, delta_time):
        """ Movement and game logic """
        self.update_player_position(delta_time)
        self.update_camera(delta_time)
        self.physics_engine.update()

    def debug(self):
        print(self.player_sprite.position, self.player_sprite.points)
