import os

import arcade

from . import mission_manager
from .definitions import ASSETS_PATH


class Objective:
    """ Holds information needed in order to draw a Objective/mission """

    def __init__(self, manager: mission_manager, name: str, amount: int, category: str):
        self.manager = manager
        self.name = name
        self.amount = amount
        self.category = category

    def get_progressbar_texture(self, scale: float = 1) -> arcade.Texture:
        """ Gets the texture that should be used for this objective"""
        index = 7 * int(self.manager.get_progress(self.category) / self.amount)
        return arcade.load_texture(os.path.join(ASSETS_PATH, "progressbar.png"), 0, index * 24, 96, 24, scale=scale)
