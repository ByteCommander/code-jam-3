import os

import arcade
from arcade.sprite import AnimatedWalkingSprite

from .definitions import ASSETS_PATH


class CustomAnimatedSprite(AnimatedWalkingSprite):
    """Base class for managing an animated sprite"""

    def __init__(self,
                 file_name: str,
                 width: float,
                 height: float,
                 scale: float = 1,
                 columns: int = 3,
                 idle_index: int = 1,
                 offset_x: float = 0,
                 offset_y: float = 0,
                 rows=None,
                 animations=None,
                 collision_points=None):
        """
        Args:
            :file_name: filename relative to project/assets
            :columns: List which maps the columns to directions.
                [Up, Left, Right, Down]
        """
        super().__init__(scale=scale)
        if rows is None:
            rows = [0, 1, 2, 3]
        self.image_width = width
        self.image_height = height
        self.offset_y = offset_y
        self.offset_x = offset_x
        self.columns = columns
        self.file = os.path.join(ASSETS_PATH, file_name)

        # Load walking animations
        self.walk_down_textures = self._load_column(rows[0])
        self.walk_left_textures = self._load_column(rows[1])
        self.walk_right_textures = self._load_column(rows[2])
        self.walk_up_textures = self._load_column(rows[3])

        # Set idle frame
        self.stand_up_textures = [self.walk_up_textures[idle_index]]
        self.stand_left_textures = [self.walk_left_textures[idle_index]]
        self.stand_right_textures = [self.walk_right_textures[idle_index]]
        self.stand_down_textures = [self.walk_down_textures[idle_index]]

        if animations:  # TODO
            print("WARNING: Custom character animation loaded, but feature is not implemented yet. Using default.")

        if collision_points:
            self.points = [(x * scale, y * scale) for (x, y) in collision_points]

        self.update_animation()

    def _load_column(self, row):
        locations = [
            [self.offset_x + self.image_width * index, self.offset_y + row * self.image_height, self.image_width,
             self.image_height]
            for index in range(self.columns)]
        return arcade.load_textures(self.file, locations)

    def update_animation(self):
        # TODO: replace with custom implementation using animation settings loaded from the TSX file
        super().update_animation()

        if self.change_x == 0 and self.change_y == 0:
            if self.state == arcade.FACE_UP:
                self.texture = self.stand_up_textures[0]
            elif self.state == arcade.FACE_DOWN:
                self.texture = self.stand_down_textures[0]

        self.width = self.texture.width * self.scale
        self.height = self.texture.height * self.scale
